
let num = 2
let getCube = num ** 3

console.log(`The cube of ${num} is ${getCube}`)



const address = ["NEC", "East Ave.", "Brgy. Pinyahan", "Quezon City", "Metro Manila"];
const [building, street, brgy, city, province] = address;

console.log(`I live at ${building} ,${street}, ${brgy} ,${city}, ${province}`);



const animal = {
	name: " Tyrannosaurus rex ",
	species: "Dinosaur",
	weight: "upto 14 Metric Tons",
	height: "upto 12.4 meters" 
}

const {name, species, weight, height} = animal
console.log(`${name} was a ${species} that can weight ${weight} and can grow ${height}`)


let numbers = [1, 2, 3, 4, 5];
numbers.forEach(number => console.log(number));



let reduceNumber = numbers.reduce((x, y) => x + y);
console.log(reduceNumber)




class Dog {
	constructor(name, age, breed) {
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

const myDog = new Dog("Chobbie", 3, "Corgi Mixed");
console.log(myDog);


